#ifndef CAMERAH
#define CAMERAH
#include "Ray.h"

/*
	Authors: Ram�n Wilhelm & Oliver Kovarna
*/

class Camera
{
public:

	// Leerer Konstruktor, bei dem die Standartwerte zugewiesen werden.
	Camera() {
		lower_left_corner = Vec3(-2.0, -1.0, -1.0);
		horizontal = Vec3(4.0, 0.0, 0.0);
		vertical = Vec3(0.0, 2.0, 0.0);
		origin = Vec3(0.0, 0.0, 0.0);
	}


	// Kamera-Konstruktor, dem man den niedrigen linken Punkt, den horizontalen Vektor, den
	// vertikalen Vektor und den Ursprung als Parameter �bergeben kann.
	Camera(Vec3 llc, Vec3 h, Vec3 v, Vec3 o) {
		lower_left_corner = llc;
		horizontal = h;
		vertical = v;
		origin = o;
	}

	// Gebe den Strahl bei seiner aktuellen Position zur�ck
	Ray get_ray(float u, float v)
	{
		return Ray(origin, lower_left_corner + u * horizontal + v * vertical - origin);
		// Der Ursprung muss abgezogen werden, damit die Plane sich mit der Kamera verschiebt.
	}

	// Ursprung
	Vec3 origin;

	// Der niedrige linke Punkt des Koordinatensystems
	Vec3 lower_left_corner;

	// Horizontale Verschiebung
	Vec3 horizontal;

	// Verticale Verschiebung
	Vec3 vertical;
};

#endif