#ifndef HITTABLEH
#define HITTABLEH

/*
    Authors: Ram�n Wilhelm & Oliver Kovarna
*/

#include "Ray.h"
// Der Compiler wird informiert, dass der Material Pointer im Struct auf die Material-Klasse verweist.
// Ein '#include "Material.h"' kann nicht verwendet werden, weil in der Material.h bereits die 'HitTable.h' inkludiert wurde. 
// Wird "Material.h" trotzdem in der "HitTable.h" inkludiert, wird es wegen der gegenseitigen inkludierten Abh�ngigkeiten zu Fehlern im Compiler f�hren.
class Material;

// Ein Stuct, der den Normalen Vektor, die t und einen Punkt speichert.
// Der struct aus C wurde hier verwendet, um auf viele Parameterangeben in Klassen und Memberfunktionen zu verzichten.
struct hit_record {
    // t
    float t = 0;
    //p ist ein Punkt
    Vec3 p;
    // Normalenvektor
    Vec3 normal;
    // Pointer, der auf Unterklassen von Material verweist.
    Material *matr_ptr = NULL;
};

/*
    Die Oberklasse enth�lt eine abstrakte Funktion, die jede Unterklasse erbt. Ansonsten enth�lt sie nichts.
*/
class HitTable {
public:
    virtual bool hit(const Ray& r, float t_min, float t_max, hit_record& rec) const = 0;
};

#endif