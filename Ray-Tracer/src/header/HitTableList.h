#ifndef HITTABLELISTH
#define HITTABLELISTH

#include "HitTable.h"

/*
    Authors: Ram�n Wilhelm & Oliver Kovarna
*/

/*
    Die HitTableList ist eine Unterklasse von HitTable.
    Sie hat als Datenfelder nur die HitTableList, die ein Array f�r Objekte ist, die ebenfalls eine HitTable als Oberklasse haben.
    Sie hat nur eine Funktion, die pr�ft, ob sich der Strahl innerhalb eines dieser Objekte befindet.

*/
class HitTableList : public HitTable {
public:

    // Leerer Konstruktor
    HitTableList() 
    {
    }

    // Ein Konstruktor, dem eine Referenz auf ein Array �bergeben wird, welches Objekte speichert, die von der Oberklasse HitTable erben.
    // Der zweite Parameter ist die Anzahl der gespeicherten Elemente.
    HitTableList(HitTable** l, int n) 
    { 
        list = l; 
        list_size = n; 
    }

    // Abstrakte (virtuelle) Memberfunktion von HitTable, die f�r die HitTableList �berschrieben wird.
    virtual bool hit(const Ray& r, float tmin, float tmax, hit_record& rec) const;

    // Public Datenfelder der Objektliste und der Anzahl der Listeneintr�ge in list.
    HitTable** list;
    int list_size;
};

// Die Hit-Funktion iteriert die Liste durch und pr�ft bei jedem einzelnen Objekt, ob der Strahl einen beliebigen Pixel eines Objektes getroffen hat.
// Es wird entweder ein True oder ein False zur�ckgegeben.
bool HitTableList::hit(const Ray& r, float t_min, float t_max, hit_record& rec) const {

    hit_record temp_rec;
    bool hit_anything = false;
    double closest_so_far = t_max;
    for (int i = 0; i < list_size; i++) {
        if (list[i]->hit(r, t_min, closest_so_far, temp_rec)) {
            hit_anything = true;
            closest_so_far = temp_rec.t;
            rec = temp_rec;
        }
    }
    return hit_anything;
}

#endif