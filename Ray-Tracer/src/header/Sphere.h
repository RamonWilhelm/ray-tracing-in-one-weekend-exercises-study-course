#ifndef SPHEREH
#define SPHEREH

#include "HitTable.h"

/*
    Authors: Ram�n Wilhelm & Oliver Kovarna
*/

class Sphere: public HitTable
{
	public:

		Sphere() 
		{
		}

		virtual bool hit(const Ray& r, float t_min, float t_max, hit_record& rec) const;

        Sphere(Vec3 cen, float r)
        {
            center = cen;
            radius = r;
        };

		Sphere(Vec3 cen, float r, Material *m) 
		{
            center = cen;
            radius = r;

            // Der Kugel wird das Material �bergeben.
            matr_ptr = m;
           
		};

		Vec3 getCenter()
		{
			return center;
		}

		float getRadius()
		{
			return radius;
		}

        Material* matr_ptr;

	private:
		Vec3 center;
		float radius;

};


/*
    Pr�ft, ob der Strahl einen Punkt innerhalb der Kugel getroffen hat.
    Der Punkt, t und der dazugeh�rige Normalenvektor werden in einem Struct zwischengespeichert.
    Die Funktion liefert entweder True oder False zur�ck.
*/
bool Sphere::hit(const Ray& r, float t_min, float t_max, hit_record& rec) const {
    Vec3 oc = r.origin() - center;
    float a = dot(r.direction(), r.direction());
    float b = dot(oc, r.direction());
    float c = dot(oc, oc) - radius * radius;
    float discriminant = b * b - a * c;
    if (discriminant > 0) {
        float temp = (-b - sqrt(discriminant)) / a;
        if (temp < t_max && temp > t_min) {
            rec.t = temp;
            rec.p = r.point_at_parameter(rec.t);
            rec.normal = (rec.p - center) / radius;

            // Der Kugel wird ebenso das Material zugewiesen.
            rec.matr_ptr = matr_ptr;

            return true;
        }
        temp = (-b + sqrt(discriminant)) / a;
        if (temp < t_max && temp > t_min) {
            rec.t = temp;
            rec.p = r.point_at_parameter(rec.t);
            rec.normal = (rec.p - center) / radius;

            // Der Kugel wird ebenso das Material zugewiesen.
            rec.matr_ptr = matr_ptr;



            return true;
        }
    }
    return false;
}

#endif