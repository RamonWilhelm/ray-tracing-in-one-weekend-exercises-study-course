#ifndef MATERIALH
#define MATERIALH

#include "Ray.h"
#include "HitTable.h"
#include "Random.h"

/*
    Authors: Ram�n Wilhelm & Oliver Kovarna
*/

// Der globale Zugriff auf die Random_In_Unit_Sphere-Funktion wurde hier auf diesem Bereich ausgelagert.
Vec3 random_in_unit_sphere() {
    Vec3 p;
    do {
        p = 2.0 * Vec3(random_double(), random_double(), random_double()) - Vec3(1, 1, 1);
    } while (p.squared_length() >= 1.0);
    return p;
}

// Berechnet die Reflexion der Metal-Oberfl�che gem�� der Formel (v + 2B) -> roter Pfeil (Reflexion)
// B ist der Richtungsvektor. v ist ein Einheitsvektor. 
// Die L�nge von B sollte dot(v, N) sein.
// Bemerkung: Bei weiche Metalloberfl�chen (smooth metals) wird der Strahl nicht zuf�llig (randomly) verteilt (scattered).
Vec3 reflect(const Vec3& v, const Vec3& n) {
    return v - 2 * dot(v, n) * n;
}

// Code for the refraction (Dielectric)
bool refract(const Vec3& v, const Vec3& n, float ni_over_nt, Vec3& refracted) {
    Vec3 uv = unit_vector(v);
    float dt = dot(uv, n);
    float discriminant = 1.0 - ni_over_nt * ni_over_nt * (1 - dt * dt);
    if (discriminant > 0) {
        refracted = ni_over_nt * (uv - n * dt) - n * sqrt(discriminant);
        return true;
    }
    else
        return false;
}

// Named after Christophe Schlick
// For removing the sleeper bug (no reflections within a mirror).
float schlick(float cosine, float ref_idx) {
    float r0 = (1 - ref_idx) / (1 + ref_idx);
    r0 = r0 * r0;
    return r0 + (1 - r0) * pow((1 - cosine), 5);
}

// Hier wird die abstrakte Klasse 'Material' deklariert.
class Material
{
public:
    virtual bool scatter(
        const Ray& r_in, const hit_record& rec, Vec3& attenuation, Ray& scattered) const = 0;
};

// Hier beginnt die Deklaration der Unterklasse 'Lambertian'
// Lambertian bedeuted 'Diffuses Licht' und 'diffus' bedeutet ebenso 'Verteilung'.
class Lambertian : public Material {
public:
    Lambertian(const Vec3& a) : albedo(a) {}
    virtual bool scatter(const Ray& r_in, const hit_record& rec, Vec3& attenuation, Ray& scattered) const;

    // 'albedo' bedeuted soviel wie 'R�ckstrahl'.
    // albedo ist eine public Membervariable.
    Vec3 albedo;
};

// Hier wird die Memberfunktion 'scatter' von 'Lambertian' definiert.
// Die Funktion gibt nur ein True zur�ck!
bool Lambertian::scatter(const Ray& r_in, const hit_record& rec, Vec3& attenuation, Ray& scattered) const {
    // Definiert einen weiteren Punkt, auf den der Strahl verteilt (scattered) werden soll.
    Vec3 target = rec.p + rec.normal + random_in_unit_sphere();
    // Definiert den Verteilungsstrahl.
    // Es handelt sich hier um eine Referenz!
    scattered = Ray(rec.p, target - rec.p);
    // Strahl auf den wieder zur�ckgestrahlt werden soll.
    // Es handelt sich hier um eine Referenz!
    attenuation = albedo;
    return true;
}

// Hier beginnt die Deklaration der Unterklasse 'Metal'.
// Konstruktor f�r die Metalloberfl�che
// Je gr��er die Kugeln, um so 'fusseliger' sehen die aus.
// Deswegen wird ein sogenannter 'Fuzzyparameter' eingef�hrt, um diesen Effekt zu verhindern.
class Metal : public Material {
public:

    Metal(const Vec3& a) : albedo(a) {
    }

    Metal(const Vec3& a, float f) : albedo(a) {
    
        if (f < 1)
        {
            fuzz = f;
        }
        else
        {
            fuzz = 1;
        }
    
    }
    virtual bool scatter(const Ray& r_in, const hit_record& rec, Vec3& attenuation, Ray& scattered) const;
    // 'albedo' bedeuted soviel wie 'R�ckstrahl'. albedo ist eine public Membervariable.
    Vec3 albedo;
    // Adding fuzziness parameter
    float fuzz;
};

// Hier wird die Memberfunktion 'scatter' von 'Metal' definiert.
// Die Funktion gibt nur ein True zur�ck!
bool Metal::scatter(const Ray& r_in, const hit_record& rec, Vec3& attenuation, Ray& scattered) const {
    // Berechnet einen reflektierenden Vektor.
    Vec3 reflected = reflect(unit_vector(r_in.direction()), rec.normal);
    // Sendet den Strahl in eine bestimmte Richtung.
    // Es handelt sich hier um eine Referenz!
    scattered = Ray(rec.p, reflected + fuzz * random_in_unit_sphere());
    // �bergibt den R�ckstrahl als Referenz.
    // Es handelt sich hier um eine Referenz!
    attenuation = albedo;

    return (dot(scattered.direction(), rec.normal) > 0);
}

// Dielectric Material
class Dielectric : public Material {
public:
    Dielectric(float ri) : ref_idx(ri) {}
    virtual bool scatter(const Ray& r_in, const hit_record& rec,
        Vec3& attenuation, Ray& scattered) const {
        Vec3 outward_normal;
        Vec3 reflected = reflect(r_in.direction(), rec.normal);
        float ni_over_nt;

        // Attenuation is always 1 - the glass surface absorbs nothing
        // It also kills the blue channel (1.0, 1.0, 0.0).
        // For remaining the blue channel use (1.0, 1.0, 0.0) for the attenuation.
        attenuation = Vec3(1.0, 1.0, 1.0);
        Vec3 refracted;

        float reflect_prob;
        float cosine;

        if (dot(r_in.direction(), rec.normal) > 0) {
            outward_normal = -rec.normal;
            ni_over_nt = ref_idx;
            //cosine = dot(r_in.direction(), rec.normal) / r_in.direction().length();
            cosine = ref_idx * dot(r_in.direction(), rec.normal) / r_in.direction().length();
            //std::cout << "Cosine: " << cosine << std::endl;
        }
        else {
            outward_normal = rec.normal;
            ni_over_nt = 1.0 / ref_idx;
           // std::cout << "ni_over_nt: " << ni << std::endl;
            cosine = -dot(r_in.direction(), rec.normal) / r_in.direction().length();
           // std::cout << "Cosine: " << cosine << std::endl;
        }

        if (refract(r_in.direction(), outward_normal, ni_over_nt, refracted)) {
            //scattered = Ray(rec.p, refracted);
            // std::cout << "Cosine: " << cosine << " ist eine Refraction!!!" << std::endl;
            reflect_prob = schlick(cosine, ref_idx);
        }
        else {
            //scattered = Ray(rec.p, reflected);
            //return false;
            reflect_prob = 1.0;
        }

        if (random_double() < reflect_prob)
        {
            //std::cout << "Cosine: " << cosine << std::endl;
            scattered = Ray(rec.p, reflected);
        }
        else
        {
            scattered = Ray(rec.p, refracted);
        }

        return true;
    }

    float ref_idx;
};
#endif