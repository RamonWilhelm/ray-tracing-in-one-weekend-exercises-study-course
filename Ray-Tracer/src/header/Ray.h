#ifndef RAYH
#define RAYH
#include "Vec3.h"

/*
    Authors: Ram�n Wilhelm & Oliver Kovarna
*/


class Ray
{
public:

    // Leerer Konstruktor
    Ray()
    {
    }

    // Konstruktor, indem man als Parameter den Ursprung a und die Strahlrichtung b angibt.
    Ray(const Vec3& a, const Vec3& b)
    {
        A = a;
        B = b;
    }

    // Gibt eine Kopie vom Ursprung zur�ck.
    Vec3 origin() const
    {
        return A;
    }

    // Gibt eine Kopie der Richtung zur�ck, wo der Strahl hingeht.
    Vec3 direction() const
    {
        return B;
    }

    // Berechnet den Punkt, auf den der Strahl hinzeigt.
    // t muss ein Wert zwischen 0 und 1 sein.
    Vec3 point_at_parameter(float t) const
    {
        return A + t * B;
    }


    Vec3 A;
    Vec3 B;
};


#endif