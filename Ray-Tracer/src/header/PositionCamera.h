#ifndef POSCAMERAH
#define POSCAMERAH

#define _USE_MATH_DEFINES

#include "Ray.h"
#include <cmath>

/*
	Authors: Ram�n Wilhelm & Oliver Kovarna
*/

class PositionCamera
{
public:



	// Leerer Konstruktor, bei dem die Standartwerte zugewiesen werden.
	PositionCamera(Vec3 lookfrom, Vec3 lookat, Vec3 vup, float vfov, float aspect) {
		// vfov (vertical field of view)
		// Formular: h = tan(theta/2)
		float theta = vfov * M_PI / 180; // Angle
		float half_height = tan(theta/2);
		float half_width = aspect * half_height;
		origin = lookfrom;
		w = unit_vector(lookfrom - lookat);
		u = unit_vector(cross(vup, w));
		v = cross(w, u);

		lower_left_corner = origin - half_width * u - half_height * v - w;
		horizontal = 2 * half_width * u;
		vertical = 2 * half_height * v;
	}

	// Gebe den Strahl bei seiner aktuellen Position zur�ck
	Ray get_ray(float s, float t)
	{
		return Ray(origin, lower_left_corner + s * horizontal + t * vertical - origin);
		// Der Ursprung muss abgezogen werden, damit die Plane sich mit der Kamera verschiebt.
	}

	// Ursprung
	Vec3 origin;

	// Der niedrige linke Punkt des Koordinatensystems
	Vec3 lower_left_corner;

	// Horizontale Verschiebung
	Vec3 horizontal;

	// Verticale Verschiebung
	Vec3 vertical;

	Vec3 u;
	Vec3 v;
	Vec3 w;
};

#endif