#ifndef COLORH
#define COLORH

#include <float.h>
#include "HitTableList.h"

/*
    Authors: Ram�n Wilhelm & Oliver Kovarna
*/

class Color
{
    public:
        static bool hit_sphere(const Vec3& center, float radius, const Ray& r)
        {
            Vec3 oc = r.origin() - center;
            float a = dot(r.direction(), r.direction());
            float b = 2.0 * dot(oc, r.direction());
            float c = dot(oc, oc) - radius * radius;
            float discriminant = b * b - 4 * a * c;
            return (discriminant > 0);
        }

        /*
            Zeichnet Hintergrund und Kugeln.
            Sollten sich in dem Bild Kugeln befinden, werden ihre Pixel rot gezeichnet.
        */

        static Vec3 color(const Vec3& center, float radius, const Ray& r)
        {
            if (hit_sphere(center, radius, r))
            {
                return Vec3(1, 0, 0);
            }

            Vec3 unit_direction = unit_vector(r.direction());
            float t = 0.5 * (unit_direction.y() + 1.0);
            return (1.0 - t) * Vec3(1.0, 1.0, 1.0) + t * Vec3(0.5, 0.7, 1.0);
            //return Vec3(0.5, 0.7, 1.0);
        }

        /*
            Zeichnet nur einen Hintergrund mit Wei�-Blauen Farbverlauf (Wei�er Blende in der Mitte)
        */

        static Vec3 color(const Ray& r)
        {
            Vec3 unit_direction = unit_vector(r.direction());
            float t = 0.5 * (unit_direction.y() + 1.0);
            return (1.0 - t) * Vec3(1.0, 1.0, 1.0) + t * Vec3(0.5, 0.7, 1.0);
            //return Vec3(0.5, 0.7, 1.0);
        }

        /*
            Color Beispiel f�r mehrere Objekte.
        */

        /*
        static Vec3 color(const Ray& r, HitTable* world, int depth)
        {
            // Erstellt ein leeres Struct aus der "HitTable.h", die den Punkt und den Vektor speichert, sowie das t.

            hit_record rec;

            // Using 0.001 for avoiding the shadow acne problem
            if (world->hit(r, 0.001, FLT_MAX, rec))
            {
                Ray scattered;
                Vec3 attenuation;
                
                if (depth < 50 && rec.matr_ptr->scatter(r, rec, attenuation, scattered)) {
                    return attenuation * color(scattered, world, depth+1);
                }
                else {
                    return Vec3(0, 0, 0);
                }
            }
            else
            {
                // Hintergrund
                Vec3 unit_direction = unit_vector(r.direction());
                float t = 0.5 * (unit_direction.y() + 1.0);
                // Beide Vektoren sind Farbwerte
                return (1.0 - t) * Vec3(1.0, 1.0, 1.0) + t * Vec3(0.5, 0.7, 1.0);
            }
        }
        */

        /*
        static Vec3 Materialcolor(const Ray& r, HitTable* world)
        {
            // Erstellt ein leeres Struct aus der "HitTable.h", die den Punkt und den Vektor speichert, sowie das t.
            hit_record rec;

            // Using 0.001 for avoiding the shadow acne problem
            if (world->hit(r, 0.001, FLT_MAX, rec))
            {
                // Berechnet Farbwert f�r diesen Punkt, mithilfe der Normale.
                Vec3 target = rec.p + rec.normal + random_in_unit_sphere();
                return 0.5 * Materialcolor(Ray(rec.p, target - rec.p), world);
            }
            else
            {
                // Hintergrund
                Vec3 unit_direction = unit_vector(r.direction());
                float t = 0.5 * (unit_direction.y() + 1.0);
                // Beide Vektoren sind Farbwerte
                return (1.0 - t) * Vec3(1.0, 1.0, 1.0) + t * Vec3(0.5, 0.7, 1.0);
            }
        }
        */

        
        static Vec3 colorNormal(const Ray& r, HitTable* world)
        {
            // Erstellt ein leeres Struct aus der "HitTable.h", die den Punkt und den Vektor speichert, sowie das t.
            hit_record rec;

            if (world->hit(r, 0.0, FLT_MAX, rec))
            {
                // Berechnet Farbwert f�r diesen Punkt, mithilfe der Normale.
                // Ohne record -> Vec3 N = unit_vector(r.point_at_parameter(t) - Vec3(0,0,-1));
                return 0.5 * Vec3(rec.normal.x() + 1, rec.normal.y() + 1, rec.normal.z() + 1);
            }
            else
            {
                Vec3 unit_direction = unit_vector(r.direction());
                float t = 0.5 * (unit_direction.y() + 1.0);
                return (1.0 - t) * Vec3(1.0, 1.0, 1.0) + t * Vec3(0.5, 0.7, 1.0);
            }
        }
        
        static Vec3 colorWithoutNormal(const Ray& r, HitTable* world)
        {
            // Erstellt ein leeres Struct aus der "HitTable.h", die den Punkt und den Vektor speichert, sowie das t.
            hit_record rec;

            if (world->hit(r, 0.0, FLT_MAX, rec))
            {
                // Berechnet Farbwert f�r diesen Punkt, mithilfe der Normale.
                return Vec3(1, 0, 0);
            }
            else
            {
                Vec3 unit_direction = unit_vector(r.direction());
                float t = 0.5 * (unit_direction.y() + 1.0);
                return (1.0 - t) * Vec3(1.0, 1.0, 1.0) + t * Vec3(0.5, 0.7, 1.0);
            }
        }

};
#endif