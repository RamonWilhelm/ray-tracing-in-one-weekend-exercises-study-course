#ifndef VEC3H
#define VEC3H

#include <iostream>
#include <math.h>
#include <stdlib.h>

/*
    Authors: Ram�n Wilhelm & Oliver Kovarna
*/

class Vec3 {
public:

    // Leerer Konstruktor der Klasse Vec3
    Vec3()
    {
    }

    // Vec3-Konstruktor, bei dem die einzelnen Komponenten als Parameter �bergeben werden.
    // Die Komponenten bekommen Float-Werte, falls die Zahlen dividiert werden.
    Vec3(float e0, float e1, float e2)
    {
        e[0] = e0;
        e[1] = e1;
        e[2] = e2;
    }

    // Das Schl�sselwort inline verbessert die Performance und Geschwindigkeit im Compiler

    // Gibt die Komponente X zur�ck.
    inline float x() const
    {
        return e[0];
    }

    // Gibt die Komponente Y zur�ck.
    inline float y() const
    {
        return e[1];
    }

    // Gibt die Komponente Z zur�ck.
    inline float z() const
    {
        return e[2];
    }

    // Wie x(), nur eine alternative Funktion, falls mit RGB-Farbwerten gearbeitet wird.
    inline float r() const
    {
        return e[0];
    }

    // Wie y(), nur eine alternative Funktion, wenn mit RGB-Werten gearbeitet wird.
    inline float g() const
    {
        return e[1];
    }

    // Wie z(), nur andere Funktion, wenn mit RGB-Werten im Vektor gearbeitet wird.
    inline float b() const
    {
        return e[2];
    }

    // Bei dieser Funktion wird einfach ein positives Vorzeichen davor gesetzt.
    inline const Vec3& operator+() const
    {
        return *this;
    }

    // Bei einem negativen Vorzeichen werden alle Komponenten negiert.
    inline Vec3 operator-() const
    {
        return Vec3(-e[0], -e[1], -e[2]);
    }

    // Es wird eine Kopie von einem bestimmten Index im Vektor zur�ckgegeben. Call-By-Value.
    inline float operator[](int i) const
    {
        return e[i];
    }

    // Es wird eine Referenz von einem bestimmten Element zur�ckgegeben. Call-By-Reference.
    inline float& operator[](int i)
    {
        return e[i];
    }

    // Arithmetische Funktionsk�pfe werden hier deklariert (Vorw�rtsdeklaration).
    // Au�erhalb der Klasse Vec3 werden die eigentlichen Funktionen erst definiert.
    inline Vec3& operator+=(const Vec3& v2);
    inline Vec3& operator-=(const Vec3& v2);
    inline Vec3& operator*=(const Vec3& v2);
    inline Vec3& operator/=(const Vec3& v2);
    inline Vec3& operator*=(const float t);
    inline Vec3& operator/=(const float t);

    // Gibt die L�nge (Betrag) von einem Vektor zur�ck.
    // L�nge berechnet sich aus der Quadratwurzel aus der Addition der quadrierten Komponenten
    // (Satz des Pythagoras)
    inline float length() const
    {
        return sqrt(e[0] * e[0] + e[1] * e[1] + e[2] * e[2]);
    }

    // Gibt nur die quadrierte L�nge zur�ck (ohne Wurzelergebnis)
    inline float squared_length() const
    {
        return e[0] * e[0] + e[1] * e[1] + e[2] * e[2];
    }

    // Berechnung eines Vektors zu einem Einheitsvektor
    inline void make_unit_vector()
    {
        float k = 1.0 / sqrt(e[0] * e[0] + e[1] * e[1] + e[2] * e[2]);
        e[0] *= k;
        e[1] *= k;
        e[2] *= k;
    }

    // Optionale �ffentliche 4. Komponente
    // Ideal f�r RGBA-Werte.
    float e[3];
};



// Diese Funktion wird ben�tigt, um einen Vektor als Eingabe eingeben zu k�nnen.
inline std::istream& operator>>(std::istream& is, Vec3& t) {
    is >> t.e[0] >> t.e[1] >> t.e[2];
    return is;
}

// Mit dieser Funktion kann ein Vektor ausgegeben werden, z.B. in der Konsole.
inline std::ostream& operator<<(std::ostream& os, const Vec3& t) {
    os << t.e[0] << " " << t.e[1] << " " << t.e[2];
    return os;
}


// Funktion, um zwei Vektoren mit dem "+"-Operator miteinander addieren zu k�nnen.
inline Vec3 operator+(const Vec3& v1, const Vec3& v2) {
    return Vec3(v1.e[0] + v2.e[0], v1.e[1] + v2.e[1], v1.e[2] + v2.e[2]);
}

// Funktion, um zwei Vektoren mit dem "-"-Operator miteinander subtrahieren zu k�nnen.
inline Vec3 operator-(const Vec3& v1, const Vec3& v2) {
    return Vec3(v1.e[0] - v2.e[0], v1.e[1] - v2.e[1], v1.e[2] - v2.e[2]);
}

// Funktion, um zwei Vektoren mit dem "*"-Operator miteinander multiplizieren zu k�nnen.
inline Vec3 operator*(const Vec3& v1, const Vec3& v2) {
    return Vec3(v1.e[0] * v2.e[0], v1.e[1] * v2.e[1], v1.e[2] * v2.e[2]);
}

// Funktion, um zwei Vektoren mit dem "/"-Operator miteinander dividieren zu k�nnen.
inline Vec3 operator/(const Vec3& v1, const Vec3& v2) {
    return Vec3(v1.e[0] / v2.e[0], v1.e[1] / v2.e[1], v1.e[2] / v2.e[2]);
}

// Funktion, um einen Vektor und einen Faktor mit dem "*"-Operator zu multiplizieren.
inline Vec3 operator*(float t, const Vec3& v) {
    return Vec3(t * v.e[0], t * v.e[1], t * v.e[2]);
}

// Funktion, um einen Vektor und einen Faktor mit dem "/"-Operator zu dividieren.
inline Vec3 operator/(Vec3 v, float t) {
    return Vec3(v.e[0] / t, v.e[1] / t, v.e[2] / t);
}

// Funktion, um einen Vektor und einen Faktor mit dem "*"-Operator zu multiplizieren, aber in einer anderen Parameterreihenfolge.
inline Vec3 operator*(const Vec3& v, float t) {
    return Vec3(t * v.e[0], t * v.e[1], t * v.e[2]);
}

// Berechnet das Skalarprodukt zwischen zwei Vektoren.
inline float dot(const Vec3& v1, const Vec3& v2) {
    return v1.e[0] * v2.e[0]
        + v1.e[1] * v2.e[1]
        + v1.e[2] * v2.e[2];
}

// Berechnet das Kreuzprodukt zwischen zwei Vektoren.
inline Vec3 cross(const Vec3& v1, const Vec3& v2) {
    return Vec3(v1.e[1] * v2.e[2] - v1.e[2] * v2.e[1],
        v1.e[2] * v2.e[0] - v1.e[0] * v2.e[2],
        v1.e[0] * v2.e[1] - v1.e[1] * v2.e[0]);
}

// Mit dieser Funktion kann man auf einen Vektor den "+="-Operator mit einem anderen Vektor anwenden.
inline Vec3& Vec3::operator+=(const Vec3& v) {
    e[0] += v.e[0];
    e[1] += v.e[1];
    e[2] += v.e[2];
    return *this;
}

// Mit dieser Funktion kann man auf einen Vektor den "*="-Operator mit einem anderen Vektor anwenden.
inline Vec3& Vec3::operator*=(const Vec3& v) {
    e[0] *= v.e[0];
    e[1] *= v.e[1];
    e[2] *= v.e[2];
    return *this;
}
// Mit dieser Funktion kann man auf einen Vektor den "/="-Operator mit einem anderen Vektor anwenden.
inline Vec3& Vec3::operator/=(const Vec3& v) {
    e[0] /= v.e[0];
    e[1] /= v.e[1];
    e[2] /= v.e[2];
    return *this;
}

// Mit dieser Funktion kann man auf einen Vektor den "-="-Operator mit einem anderen Vektor anwenden.
inline Vec3& Vec3::operator-=(const Vec3& v) {
    e[0] -= v.e[0];
    e[1] -= v.e[1];
    e[2] -= v.e[2];
    return *this;
}

// Mit dieser Funktion kann man auf einen Vektor den "*="-Operator mit einem Faktor anwenden.
inline Vec3& Vec3::operator*=(const float t) {
    e[0] *= t;
    e[1] *= t;
    e[2] *= t;
    return *this;
}

// Mit dieser Funktion kann man auf einen Vektor den "/="-Operator mit einem Faktor anwenden.
inline Vec3& Vec3::operator/=(const float t) {
    float k = 1.0f / t;

    e[0] *= k;
    e[1] *= k;
    e[2] *= k;
    return *this;
}

// Berechnet aus einem Vektor einen Einheitsvektor. 
inline Vec3 unit_vector(Vec3 v) {
    return v / v.length();
}


#endif